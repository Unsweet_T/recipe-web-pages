from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    print(recipes)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        #We should use the form to validate the values
        # an save them to the database
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("recipe_list")
            # If all goes well, we can redirect the browser
            #  to another page and leave the function
            return redirect("recipe_list")
    else:
        # Create an instance of the Django model form class
        form = RecipeForm()
    #Put the form in the context
    context = {
        "form": form,
    }
    #Render the HTML template with the form
    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    post = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        #We should use the form to validate the values
        # an save them to the database
        form = RecipeForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            # If all goes well, we can redirect the browser
            #  to another page and leave the function
            return redirect("show_recipe", id=id)
    else:
        # Create an instance of the Django model form class
        form = RecipeForm(instance=post)

    #Put the form in the context
    context = {
        "post_object": post,
        "form": form,
    }
    #Render the HTML template with the form
    return render(request, "recipes/edit.html", context)
